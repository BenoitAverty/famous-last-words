# Famous Last Words

This is a pet project used to learn some web technology. This is a website that displays fictional "famous last words" quotes.

## Why

This project gave me the opportunity to practice the following technologies: 

* AI Design. I’m not a very good UI designer, everything I do seems ugly. The UI of this project was generated with a combination of https://v0.dev and my IDE’s integrated LLM chat (jetbrains AI). It turns out not amazing, but also not bad I think.
* The AHA stack (https://ahastack.dev). There is a little bit of htmx and alpineJS for the frontent, and the backend is an Astro server-rendered project.
* Supabase. The data is hosted on a supabase instance, and there is authentication (but for a single user: me)
* Cloudflare. The project is deployed on cloudflare workers, although I had little to do here since everything is handled by the astro cloudflare adapter.

## Missing features

* People can add an unlimited amount of likes or dislikes. It could be solved with [Anonymous sign-ins](https://supabase.com/docs/guides/auth/auth-anonymous)
* Robustness and error handling. On this project I focused on things I didn’t already know how to do (plus error handling is not very motivating on a project that probably won’t have any users) so there are probably a lot of ways to break the app. Please note that the security itself should be fine. 

## How to run locally

1. Clone and install deps `git clone && npm i`
2. Setup env vars to a supabase project in a file called `.env` at the root of the project
   ```bash
   SUPABASE_URL=https://<project ref>.supabase.co
   SUPABASE_ANON_TOKEN=<supabase_data_api_jwt>
   ```
3. Run the dev server (`npm run dev`)
