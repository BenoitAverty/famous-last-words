import { createServerClient as supabaseCreateServerClient, parseCookieHeader} from "@supabase/ssr";
import type {AstroGlobal} from "astro";

export function createServerClient(astroGlobal: AstroGlobal) {
    const supabase = supabaseCreateServerClient(
        import.meta.env.SUPABASE_URL,
        import.meta.env.SUPABASE_ANON_TOKEN,
        {
            cookies: {
                getAll() {
                    return parseCookieHeader(astroGlobal.request.headers.get('Cookie') ?? '')
                },
                setAll(cookiesToSet) {
                    cookiesToSet.forEach(({ name, value, options }) =>
                        astroGlobal.cookies.set(name, value, options))
                },
            }
        }
    )

    return supabase
}

export async function getUser(astroGlobal: AstroGlobal) {
    const client = createServerClient(astroGlobal);
    const userResponse = await client.auth.getUser()
    return userResponse.data.user;
}

export async function sendLoginEmail(userEmail: string, Astro: AstroGlobal) {
    const emailRedirectTo = import.meta.env.DEV ? "http://localhost:3000/admin/verify" : "https://famous-last-words.win/admin/verify"


    const client = createServerClient(Astro);
    let result = await client.auth.signInWithOtp({
        email: userEmail,
        options: {
            shouldCreateUser: false,
            emailRedirectTo,
        }
    });

    return !result.error
}

export async function verifyCode(astroGlobal: AstroGlobal) {
    const client = createServerClient(astroGlobal)
    let searchParams = astroGlobal.url.searchParams;
    let code = searchParams.get("code") as string;
    if(code) {
        let verificationResult = await client.auth.exchangeCodeForSession(code);
        if (verificationResult) {
            return verificationResult.data.user;
        }
    }
    else {
        return null
    }
}
