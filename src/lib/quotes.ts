import { v4 as uuid } from "uuid"
import type {AstroGlobal} from "astro";
import {createServerClient} from "./auth.ts";

const supabaseUrl = import.meta.env.SUPABASE_URL
const supabaseAnonToken = import.meta.env.SUPABASE_ANON_TOKEN

type DbInteraction = {
    tid: string
    created_at: string,
    quote_tid: string,
    type: 'like' | 'dislike'
}

type DbQuote = {
    tid: string
    created_at: string
    status: 'accepted' | 'rejected' | 'submitted'
    text: string
    person: string
    person_job: string | null
    date: string | null
    interactions: DbInteraction[]
}

export type Quote = {
    status: 'accepted' | 'rejected' | 'submitted'
    tid: string
    text: string
    person: string
    person_job: string | null
    date: string | null
    likes: number,
    dislikes: number,
}

export async function getAllQuotes(astroGlobal: AstroGlobal): Promise<Quote[]> {

    const client = createServerClient(astroGlobal)

    const resp = await client.from("quotes").select("*,interactions(*)")

    // TODO typings
    return (resp.data as DbQuote[]).map(mapDbQuote);
}

export async function findQuoteByTid(quoteTid: string): Promise<Quote> {
    const resp = await fetch(`${supabaseUrl}/rest/v1/quotes?tid=eq.${quoteTid}&select=*,interactions(*)`, {
        headers: {
            'apikey': supabaseAnonToken
        }
    });
    const quotes: DbQuote[] = await resp.json();

    return mapDbQuote(quotes[0])
}

export async function getRandomQuote(): Promise<Quote> {
    const resp = await fetch(`${supabaseUrl}/rest/v1/quotes?status=eq.accepted&select=*,interactions(*)`, {
        headers: {
            'apikey': supabaseAnonToken
        }
    })
    const quotes: DbQuote[] = await resp.json()

    // return random quote
    return mapDbQuote(quotes[Math.floor(Math.random() * quotes.length)]);
}

export async function likeQuote(quoteTid: string): Promise<void> {
    await fetch(`${supabaseUrl}/rest/v1/interactions`, {
        method: 'POST',
        headers: {
            'apikey': supabaseAnonToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            tid: uuid(),
            quote_tid: quoteTid,
            type: 'like'
        })
    });
}

export async function dislikeQuote(quoteTid: string): Promise<void> {
    await fetch(`${supabaseUrl}/rest/v1/interactions`, {
        method: 'POST',
        headers: {
            'apikey': supabaseAnonToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            tid: uuid(),
            quote_tid: quoteTid,
            type: 'dislike'
        })
    });
}

export async function createQuote(quoteTid: string, text: string, person: string, personJob: string | null, date: string | null): Promise<void> {
    await fetch(`${supabaseUrl}/rest/v1/quotes`, {
        method: 'POST',
        headers: {
            'apikey': supabaseAnonToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            tid: quoteTid,
            text: text,
            person: person,
            person_job: personJob,
            date: date,
            status: 'submitted'
        })
    });
}

export async function deleteQuote(quoteTid: string, Astro: AstroGlobal): Promise<void> {
    const client = createServerClient(Astro)

    await client.from("quotes").delete().eq('tid', quoteTid)
}

export async function changeStatus(quoteTid: string, newStatus: string, Astro: AstroGlobal): Promise<Quote> {
    const client = createServerClient(Astro)

    const result = await client.from("quotes").update({
        status: newStatus
    }).eq("tid", quoteTid).select("*,interactions(*)")

    return mapDbQuote(result.data![0] as DbQuote)
}


function mapDbQuote(quote: DbQuote): Quote {
    const likes = quote.interactions.filter(interaction => interaction.type === 'like').length;
    const dislikes = quote.interactions.filter(interaction => interaction.type === 'dislike').length;

    const mappedQuote: Quote = {
        tid: quote.tid,
        text: quote.text,
        person: quote.person,
        person_job: quote.person_job,
        date: quote.date,
        likes: likes,
        dislikes: dislikes,
        status: quote.status
    };

    return mappedQuote;
}
